package com.movemet.nailart.designs

class CustomException(message: String) : Exception(message)
